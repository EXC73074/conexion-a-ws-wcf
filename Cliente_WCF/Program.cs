﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Servicio_WSDL;

namespace Cliente_WCF
{
    class Program
    {
        
        static void Main(string[] args)
        {
            IpruebaClient Cliente = new IpruebaClient();
            int numero = 0;
            do
            {
                Console.Write("Ingrese un número distinto de 0" + "\n");
                numero = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(Cliente.GetData(numero));
            }
            while(numero != 0);
            Console.Write("El programa ha finalizado");
            Console.ReadKey();

        }
    }
}
